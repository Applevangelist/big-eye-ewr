--[[
Big Eye Early Warning Radar script, moose based

new implementation by quelcertoleo
original EWRS script updated by Apple 30/11/2023
original EWRS concept by Steggles - https://github.com/Bob7heBuilder/EWRS

changelog
0.0.2 - added friendly picture and bogey dope
0.0.1 - initial mvp, supports player preferences and reports contacts on schedule
]]
-- CONSTANTS *************************************************************************************

EWR = {}
EWR.version = "0.0.2"
EWR.debug = false

--[[
{
groupId,
clientName,
playerCoalition,
reportReference,
reportCardinalHeading,
reportFrequency,
reportEnabled,
reportUnits,
playerScheduler
}
]]
EWR.playerEwrsPreferences = {}

EWR.radioMenusAdded = {}

EWR.blueDetectedUnits = SET_UNIT:New()
EWR.redDetectedUnits = SET_UNIT:New()

EWR.blueCoalition = "blue"
EWR.redCoalition = "red"
EWR.minThreatLevel = 0
EWR.maxThreatLevel = 10

EWR.referenceSelf = "self"
EWR.referenceBulls = "bulls"

EWR.unitInternationalSystem = "isu"
EWR.unitImperial = "imp"

EWR.displayMessageTime = 10
EWR.maxReportUnits = 5
EWR.highReportFrequencyPreference = EWR.displayMessageTime + 2 -- can't be less than EWRS.displayMessageTime
EWR.defaultReportFrequencyPreference = 30
EWR.lowReportFrequencyPreference = 60
EWR.defaultEnableReportPreference = true

-- prefixes used in mission editor for EWR units
EWR.blueCoalitionRadarPrefixes = { "BLUE AWACS", "BLUE CVN GROUP", "BLUE SAM", "BLUE EWR" }
EWR.redCoalitionRadarPrefixes = { "RED AWACS", "RED SAM", "RED EWR" }

-- in case of respawned units, use keepUnitNames so that they can be added to the filter dinamically
blueDetectionGroup = SET_GROUP:New():FilterPrefixes(EWR.blueCoalitionRadarPrefixes):FilterStart()
blueDetection = DETECTION_UNITS:New(blueDetectionGroup, 1000):InitDetectRadar(true):FilterCategories(
        { Unit.Category.AIRPLANE, Unit.Category.HELICOPTER })
blueDetection:Start()

redDetectionGroup = SET_GROUP:New():FilterPrefixes(EWR.redCoalitionRadarPrefixes):FilterStart()
redDetection = DETECTION_UNITS:New(redDetectionGroup, 1000):InitDetectRadar(true):FilterCategories(
        { Unit.Category.AIRPLANE, Unit.Category.HELICOPTER })
redDetection:Start()

-- UTILS / MISC (yeah, misc and utils where stuff you don't know the concern goes) ***

-- @param #DETECTION_BASE.DetectedItem
-- @return Wrapper.Unit#UNIT
function EWR.detectedItemToWrapperUnit(detectedItem)
    local detectedSet = detectedItem.Set
    local detectedUnit = detectedSet:GetFirst() -- Wrapper.Unit#UNIT
    return detectedUnit
end

-- @param Vec2 objects to calculate distance
-- @return number distance in meters
function EWR.getDistance(object1Vec2, object2Vec2)
    local xDiff = object1Vec2.x - object2Vec2.x
    local yDiff = object1Vec2.y - object2Vec2.y
    local distanceInMeters = math.sqrt(xDiff * xDiff + yDiff * yDiff)
    return distanceInMeters
end

-- @param Vec2 objectFromVec2 the vertex
-- @param Vec2 objectToVec2 the destination
-- @return angle in degrees
function EWR.getBearing(objectFromVec2, objectToVec2)
    local bearing = math.atan2(objectToVec2.y - objectFromVec2.y, objectToVec2.x - objectFromVec2.x)
    if bearing < 0 then
        bearing = bearing + 2 * math.pi
    end
    bearing = bearing * 180 / math.pi
    return UTILS.Round(bearing, -1)
end

-- @param clientName String
-- @param playerCoalition EWR.blueCoalition / EWR.redCoalition
-- @param clientReferencePreference EWR.playerEwrsPreferences["reportReference"]
-- @return Vec2 of reference (self / bulls)
function EWR.getReferenceVec2ForClient(clientName, playerCoalition, clientReferencePreference)
    local referenceVec2
    if clientReferencePreference == EWR.referenceBulls and playerCoalition == EWR.blueCoalition then
        local blueBullsCoordinate = COORDINATE.GetBullseyeCoordinate(coalition.side.BLUE)
        referenceVec2 = blueBullsCoordinate:GetVec2()
    elseif clientReferencePreference == EWR.referenceBulls and playerCoalition == EWR.redCoalition then
        local redBullsCoordinate = COORDINATE.GetBullseyeCoordinate(coalition.side.RED)
        referenceVec2 = redBullsCoordinate:GetVec2()
    else
        local clientUnit = UNIT:FindByName(clientName)
        referenceVec2 = clientUnit:GetVec2()
    end
    return referenceVec2
end

-- @param setUnit Core.Set.html##(SET_UNIT), in our case the SET_UNITS that contain detected units
-- @param referenceVec2 Vec2 of reference point
-- @return Wrapper.Unit#UNIT
function EWR.getClosestUnitInSetTo(setUnit, referenceVec2)
    if setUnit:CountAlive() == 0 then
        return nil
    else
        local closestUnit = setUnit:GetFirst()
        local closestUnitDistance = EWR.getDistance(closestUnit:GetVec2(), referenceVec2)
        setUnit:ForEachUnit(
                function(unit)
                    local detectedUnitVec2 = unit:GetVec2()
                    local distanceFromDetectedUnit = EWR.getDistance(detectedUnitVec2, referenceVec2)
                    if distanceFromDetectedUnit < closestUnitDistance then
                        closestUnitDistance = distanceFromDetectedUnit
                        closestUnit = unit
                    end
                end
        )
        return closestUnit
    end
end

-- PREFERENCES AND SCHEDULERS ***

-- @param groupId number of client group
-- @param referencePreference EWRS.playerEwrsPreferences[groupId]["reportReference"]
function EWR.setReferencePreference(groupId, referencePreference)
    if EWR.debug then
        env.info(string.format("EWRS group %s set preference refecence to %s", groupId, referencePreference))
    end
    local playerPreferences = EWR.playerEwrsPreferences[groupId]
    playerPreferences["reportReference"] = referencePreference
    local newReportScheduler = EWR.getReportSchedulerForPlayerGroup(groupId, playerPreferences)
    EWR.replacePlayerReportSchedulerWith(groupId, newReportScheduler)
    trigger.action.outTextForGroup(groupId, string.format("Reference to %s", referencePreference), 10, false)
end

-- @param groupId number of client group
-- @param isHeadingCardinal EWRS.playerEwrsPreferences[groupId]["reportCardinalHeading"]
function EWR.setCardinalHeadingPreference(groupId, isHeadingCardinal)
    if EWR.debug then
        env.info(string.format("EWRS group %s set heading refecence to %s", groupId, tostring(isHeadingCardinal)))
    end
    local playerPreferences = EWR.playerEwrsPreferences[groupId]
    playerPreferences["reportCardinalHeading"] = isHeadingCardinal
    local newReportScheduler = EWR.getReportSchedulerForPlayerGroup(groupId, playerPreferences)
    EWR.replacePlayerReportSchedulerWith(groupId, newReportScheduler)
    if isHeadingCardinal then
        trigger.action.outTextForGroup(groupId, "Heading to cardinal", 10, false)
    else
        trigger.action.outTextForGroup(groupId, "Heading to numerical", 10, false)
    end
end

-- @param groupId number of client group
-- @param unitPreference EWRS.playerEwrsPreferences[groupId]["reportUnits"]
function EWR.setUnitPreference(groupId, unitPreference)
    if EWR.debug then
        env.info(string.format("EWR group %s set unit preference to %s", groupId, unitPreference))
    end
    local playerPreferences = EWR.playerEwrsPreferences[groupId]
    playerPreferences["reportUnits"] = unitPreference
    local newReportScheduler = EWR.getReportSchedulerForPlayerGroup(groupId, playerPreferences)
    EWR.replacePlayerReportSchedulerWith(groupId, newReportScheduler)
    trigger.action.outTextForGroup(groupId, string.format("Unit to %s", unitPreference), 10, false)
end

-- @param groupId number of client group
-- @param reportFrequencyPreference EWRS.playerEwrsPreferences[groupId]["reportFrequency"]
function EWR.setReportFrequencyPreference(groupId, reportFrequencyPreference)
    if EWR.debug then
        env.info(string.format("EWR group %s set preference report frequency to %2d", groupId, reportFrequencyPreference))
    end
    local playerPreferences = EWR.playerEwrsPreferences[groupId]
    playerPreferences["reportFrequency"] = reportFrequencyPreference
    local newReportScheduler = EWR.getReportSchedulerForPlayerGroup(groupId, playerPreferences)
    EWR.replacePlayerReportSchedulerWith(groupId, newReportScheduler)
    trigger.action.outTextForGroup(groupId, string.format("Frequency to %2ds", reportFrequencyPreference), 10, false)
end

-- @param groupId number of client group
-- @param reportEnabledPreference boolean, will start or stop player scheduler in EWR.playerEwrsPreferences[groupId]["playerScheduler"]
function EWR.setReportEnabledPreference(groupId, reportEnabledPreference)
    if EWR.debug then
        env.info(string.format("EWR group %s set preference report enable %s", groupId, tostring(reportEnabledPreference)))
    end
    local playerReportScheduler = EWR.playerEwrsPreferences[groupId]["playerScheduler"]
    if reportEnabledPreference then
        trigger.action.outTextForGroup(groupId, "Enable contact report", 10, false)
        playerReportScheduler:Start()
    else
        trigger.action.outTextForGroup(groupId, "Disable contact report", 10, false)
        playerReportScheduler:Stop()
    end
    EWR.playerEwrsPreferences[groupId]["reportEnabled"] = reportEnabledPreference
end

-- @param groupId number of client group
-- @param playerPreferences EWR.playerEwrsPreferences[groupId]
-- @return #SCHEDULER
function EWR.getReportSchedulerForPlayerGroup(groupId, playerPreferences)
    local playerReportScheduler = SCHEDULER:New(nil,
            function()
                local isOnDemandReport = false
                EWR.printBogeyReport(groupId, playerPreferences["clientName"], playerPreferences["playerCoalition"], isOnDemandReport)
            end, {}, 0, playerPreferences["reportFrequency"], 0)
    return playerReportScheduler
end

-- @param groupId number of client group
-- @param #SCHEDULER the new scheduler with modified player preferences, stop the old one and start a new scheduler
function EWR.replacePlayerReportSchedulerWith(groupId, newReportScheduler)
    local playerPreferences = EWR.playerEwrsPreferences[groupId]
    local playerReportScheduler = playerPreferences["playerScheduler"]
    playerReportScheduler:Stop()
    playerPreferences["playerScheduler"] = newReportScheduler
    playerReportScheduler = playerPreferences["playerScheduler"]
    playerReportScheduler:Start()
end

-- DETECTION CALLBACKS ***
function blueDetection:OnAfterDetectedItem(From, Event, To, DetectedItem)
    local detectedUnit = EWR.detectedItemToWrapperUnit(DetectedItem)
    EWR.blueDetectedUnits:AddUnit(detectedUnit)
    if EWR.debug then
        env.info(string.format("EWR BLUE detected %s", detectedUnit:GetName()))
    end
end

function redDetection:OnAfterDetectedItem(From, Event, To, DetectedItem)
    local detectedUnit = EWR.detectedItemToWrapperUnit(DetectedItem)
    EWR.redDetectedUnits:AddUnit(detectedUnit)
    if EWR.debug then
        env.info(string.format("EWR RED detected %s", detectedUnit:GetName()))
    end
end

-- REPORTS ***

-- @param groupId number of client group
-- @param clientName string the name of the client group (and unit) as defined in Mission Editor
-- @param EWR.blueCoalition / EWR.redCoalition
function EWR.printBogeyDope(playerGroupId, clientName, playerCoalition)
    if EWR.debug then
        env.info(string.format("EWR bogey dope for group %s, %s, %s", playerGroupId, clientName, playerCoalition))
    end

    local clientUnit = UNIT:FindByName(clientName)
    local clientVec2 = clientUnit:GetVec2()
    local closestUnit
    if playerCoalition == EWR.blueCoalition then
        closestUnit = EWR.getClosestUnitInSetTo(EWR.blueDetectedUnits, clientVec2)
    else
        closestUnit = EWR.getClosestUnitInSetTo(EWR.redDetectedUnits, clientVec2)
    end

    if closestUnit ~= nil and closestUnit:IsAlive() then
        if EWR.debug then
            env.info(string.format("EWR bogey closest for group %s is %s", clientName, closestUnit:GetName()))
        end
        local clientPreferences = EWR.playerEwrsPreferences[playerGroupId]
        local unitTextReport = EWR.getTextReportForUnit(closestUnit, clientPreferences, clientVec2)
        local contactReport = REPORT:New("Bogey dope")
        contactReport:Add(unitTextReport)
        trigger.action.outTextForGroup(playerGroupId, contactReport:Text(), EWR.displayMessageTime, false)
    else
        trigger.action.outTextForGroup(playerGroupId, "clean", EWR.displayMessageTime, false)
    end
end

-- @param groupId number of client group
-- @param clientName string the name of the client group (and unit) as defined in Mission Editor
-- @param EWR.blueCoalition / EWR.redCoalition
-- @param isOnDemand boolean used to print "clean" when report has been explictly requested and no units around, if it's scheduled report then stay quiet
function EWR.printBogeyReport(groupId, clientName, playerCoalition, isOnDemand)
    if EWR.debug then
        env.info(string.format("EWR report for group %s", groupId))
    end

    if isOnDemand then
        if playerCoalition == EWR.blueCoalition then
            if EWR.blueDetectedUnits:Count() == 0 then
                trigger.action.outTextForGroup(groupId, "clean", EWR.displayMessageTime, false)
                return
            end
        else
            if EWR.redDetectedUnits:Count() == 0 then
                trigger.action.outTextForGroup(groupId, "clean", EWR.displayMessageTime, false)
                return
            end
        end
    end

    local clientPreferences = EWR.playerEwrsPreferences[groupId]
    local clientReferencePreference = clientPreferences["reportReference"]

    local contactReport = REPORT:New(string.format("Reference %s", clientReferencePreference))
    local referenceVec2 = EWR.getReferenceVec2ForClient(clientName, playerCoalition, clientReferencePreference)

    local reportUnitCounter = 1
    if playerCoalition == EWR.blueCoalition then
        EWR.blueDetectedUnits:ForEachUnitPerThreatLevel(EWR.maxThreatLevel, EWR.minThreatLevel,
                function(unit)
                    if reportUnitCounter <= EWR.maxReportUnits and unit ~= nil and unit:IsAlive() then
                        local unitTextReport = EWR.getTextReportForUnit(unit, clientPreferences, referenceVec2)
                        contactReport:Add(unitTextReport)
                        reportUnitCounter = reportUnitCounter + 1
                    end
                end
        )
    else
        EWR.redDetectedUnits:ForEachUnitPerThreatLevel(EWR.maxThreatLevel, EWR.minThreatLevel,
                function(unit)
                    if reportUnitCounter <= EWR.maxReportUnits and unit ~= nil and unit:IsAlive() then
                        local unitTextReport = EWR.getTextReportForUnit(unit, clientPreferences, referenceVec2)
                        contactReport:Add(unitTextReport)
                        reportUnitCounter = reportUnitCounter + 1
                    end
                end
        )
    end

    if playerCoalition == EWR.blueCoalition then
        if EWR.blueDetectedUnits:Count() > 0 then
            trigger.action.outTextForGroup(groupId, contactReport:Text(), EWR.displayMessageTime, false)
        end
    else
        if EWR.redDetectedUnits:Count() > 0 then
            trigger.action.outTextForGroup(groupId, contactReport:Text(), EWR.displayMessageTime, false)
        end
    end
end

-- @param playerGroupId number the group to send report to
-- @param clientName string the name of the client group (and unit) as defined in Mission Editor
-- @param playerCoalition EWR.redCoalition / EWR.blueCoalition
function EWR.printFriendliesReport(playerGroupId, clientName, playerCoalition)
    if EWR.debug then
        env.info(string.format("EWR find friendlies for %3d %s", playerGroupId, clientName))
    end
    -- cannot use { Unit.Category.AIRPLANE, Unit.Category.HELICOPTER } here, moose bug
    -- cannot use neither :FilterCategories("plane", "helicopter"), perhaps another bug?
    local friendlyUnitSet = SET_UNIT
            :New()
            :FilterActive(true)
            :FilterCoalitions(playerCoalition)
            :FilterCategories("plane")
            :FilterOnce()
    local friendlyUnitSetHelicopter = SET_UNIT
            :New()
            :FilterActive(true)
            :FilterCoalitions(playerCoalition)
            :FilterCategories("helicopter")
            :FilterOnce()
    -- remove client unit from set
    friendlyUnitSet:RemoveUnitsByName(clientName)

    if friendlyUnitSet:CountAlive() == 0 and friendlyUnitSetHelicopter:CountAlive() == 0 then
        trigger.action.outTextForGroup(playerGroupId, "no friendlies", EWR.displayMessageTime, false)
        return
    end
    -- merge the two sets
    friendlyUnitSetHelicopter:ForEachUnit(
            function(helicopter)
                friendlyUnitSet:AddUnit(helicopter)
            end
    )

    local clientUnit = UNIT:FindByName(clientName)
    local clientUnitVec2 = clientUnit:GetVec2()

    local friendliesTable = {}
    local counter = 1
    friendlyUnitSet:ForEachUnit(
            function(unit)
                friendliesTable[counter] = {}
                friendliesTable[counter].unitName = unit:GetName()
                local unitDistance = EWR.getDistance(clientUnitVec2, unit:GetVec2())
                friendliesTable[counter].distance = unitDistance
                counter = counter + 1
            end
    )

    local function sortTableByDistance(unit1, unit2)
        return unit2.distance > unit1.distance
    end
    table.sort(friendliesTable, sortTableByDistance)

    local friendlyReport = REPORT:New("Friendly picture")
    local clientPreferences = EWR.playerEwrsPreferences[playerGroupId]
    for i = 1, #friendliesTable do
        if i == EWR.maxReportUnits + 1 then
            break
        end
        local friendlyUnit = UNIT:FindByName(friendliesTable[i].unitName)
        if friendlyUnit ~= nil and friendlyUnit:IsAlive() then
            local friendlyTextReport = EWR.getTextReportForUnit(friendlyUnit, clientPreferences, clientUnitVec2)
            friendlyReport:Add(friendlyTextReport)
        end
    end
    trigger.action.outTextForGroup(playerGroupId, friendlyReport:Text(), EWR.displayMessageTime, false)
end

-- @param unit Wrapper.Unit#UNIT
-- @param clientPreferences EWRS.playerEwrsPreferences[groupId]
-- @param referenceVec2 Vec2 used to calculate relative distance, heading, tracking
-- @return String with report entry for @param unit
function EWR.getTextReportForUnit(unit, clientPreferences, referenceVec2)
    if EWR.debug then
        env.info(string.format("EWR prepare text report for %s", unit:GetName()))
    end

    -- TODO extract to function?
    local isAltitudeFromGround = false
    local unitAltitude = unit:GetAltitude(isAltitudeFromGround)
    local altitudeString
    if unitAltitude == nil then
        altitudeString = string.format("%8s", "???")
    elseif unitAltitude < 1000 then
        altitudeString = string.format("%8s", "low")
    else
        if clientPreferences["reportUnits"] == EWR.unitImperial then
            altitudeString = string.format("%05dft", UTILS.Round(UTILS.MetersToFeet(unitAltitude), -3))
        else
            altitudeString = string.format("%05dmt", UTILS.Round(unitAltitude, -2))
        end
    end
    -- angels format is for friendlies
    --local altitude = UTILS.Round(UTILS.MetersToFeet(unit:GetAltitude(isAltitudeFromGround)) / 1000, 0)

    local speedString
    if clientPreferences["reportUnits"] == EWR.unitImperial then
        speedString = string.format("%03dkts", unit:GetVelocityKNOTS())
    else
        speedString = string.format("%03dkph", unit:GetVelocityKMH())
    end

    -- TODO extract to function?
    local unitVec2 = unit:GetVec2()
    local bearing = EWR.getBearing(referenceVec2, unitVec2)
    local unitDistance = EWR.getDistance(unitVec2, referenceVec2)
    local unitDistanceToString
    if clientPreferences["reportUnits"] == EWR.unitImperial then
        unitDistanceToString = string.format("%03dnm", UTILS.Round(UTILS.MetersToNM(unitDistance), 0))
    else
        unitDistanceToString = string.format("%03dkm", UTILS.Round(unitDistance / 1000, 0))
    end
    local bearingDistanceString = string.format("%03d° for %s", bearing, unitDistanceToString)

    -- TODO extract to function?
    local heading = UTILS.Round(unit:GetHeading(), -1)
    if clientPreferences["reportCardinalHeading"] then
        heading = UTILS.BearingToCardinal(heading)
    else
        heading = string.format("%03d°", heading)
    end

    local unitTextReport = string.format(
            "%-10s | %s | %s | %s | %s",
            unit:GetNatoReportingName(),
            bearingDistanceString,
            speedString,
            altitudeString,
            heading)

    return unitTextReport
end

-- SETUP CLIENTS ***
function EWR.setupClients()
    local bluePlayerClients = SET_CLIENT:New():FilterCoalitions(EWR.blueCoalition):FilterActive(true):FilterOnce()
    bluePlayerClients:ForEachClient(
            function(client)
                local playerGroupId = client:GetClientGroupID()
                if EWR.radioMenusAdded[playerGroupId] == nil then
                    EWR.setupClientsHelper(playerGroupId, client, EWR.blueCoalition)
                end
            end
    )
    local redPlayerClients = SET_CLIENT:New():FilterCoalitions(EWR.redCoalition):FilterActive(true):FilterOnce()
    redPlayerClients:ForEachClient(
            function(client)
                local playerGroupId = client:GetClientGroupID()
                if EWR.radioMenusAdded[playerGroupId] == nil then
                    EWR.setupClientsHelper(playerGroupId, client, EWR.redCoalition)
                end
            end
    )
end

function EWR.setupClientsHelper(playerGroupId, client, playerCoalition)
    if EWR.debug then
        env.info(string.format("EWR client setup for group %s %s", playerGroupId, client:GetName()))
    end

    local reportUnit
    if playerCoalition == EWR.redCoalition then
        reportUnit = EWR.unitInternationalSystem
    else
        reportUnit = EWR.unitImperial
    end

    EWR.playerEwrsPreferences[playerGroupId] = {
        groupId = playerGroupId,
        clientName = client:GetName(),
        playerCoalition = playerCoalition,
        reportReference = EWR.referenceSelf,
        reportCardinalHeading = true,
        reportFrequency = EWR.defaultReportFrequencyPreference,
        reportEnabled = EWR.defaultEnableReportPreference,
        reportUnits = reportUnit,
        playerScheduler = nil
    }
    EWR.playerEwrsPreferences[playerGroupId]["playerScheduler"] = EWR.getReportSchedulerForPlayerGroup(playerGroupId, EWR.playerEwrsPreferences[playerGroupId])

    local isOnDemandReport = true
    missionCommands.addCommandForGroup(
            playerGroupId, "EWR Bogey Dope", nil, EWR.printBogeyDope, playerGroupId, client:GetName(), playerCoalition)
    missionCommands.addCommandForGroup(
            playerGroupId, "EWR Contact Report", nil, EWR.printBogeyReport, playerGroupId, client:GetName(), playerCoalition, isOnDemandReport)
    missionCommands.addCommandForGroup(
            playerGroupId, "EWR Friendly Picture", nil, EWR.printFriendliesReport, playerGroupId, client:GetName(), playerCoalition)

    local ewrsMenuRoot = missionCommands.addSubMenuForGroup(playerGroupId, "EWR Preferences")
    missionCommands.addCommandForGroup(
            playerGroupId, "enable contact reports", ewrsMenuRoot, EWR.setReportEnabledPreference, playerGroupId, true)
    missionCommands.addCommandForGroup(
            playerGroupId, "disable contact reports", ewrsMenuRoot, EWR.setReportEnabledPreference, playerGroupId, false)

    local ewrsReferenceMenuRoot = missionCommands.addSubMenuForGroup(
            playerGroupId, "Reference", ewrsMenuRoot)
    missionCommands.addCommandForGroup(
            playerGroupId, "bulls reference", ewrsReferenceMenuRoot, EWR.setReferencePreference, playerGroupId, EWR.referenceBulls)
    missionCommands.addCommandForGroup(
            playerGroupId, "self reference", ewrsReferenceMenuRoot, EWR.setReferencePreference, playerGroupId, EWR.referenceSelf)

    local ewrsHeadingMenuRoot = missionCommands.addSubMenuForGroup(
            playerGroupId, "Heading", ewrsMenuRoot)
    missionCommands.addCommandForGroup(
            playerGroupId, "cardinal heading", ewrsHeadingMenuRoot, EWR.setCardinalHeadingPreference, playerGroupId, true)
    missionCommands.addCommandForGroup(
            playerGroupId, "numerical heading", ewrsHeadingMenuRoot, EWR.setCardinalHeadingPreference, playerGroupId, false)

    local ewrsUnitsMenuRoot = missionCommands.addSubMenuForGroup(
            playerGroupId, "Units", ewrsMenuRoot)
    missionCommands.addCommandForGroup(
            playerGroupId, "unit imperial", ewrsUnitsMenuRoot, EWR.setUnitPreference, playerGroupId, EWR.unitImperial)
    missionCommands.addCommandForGroup(
            playerGroupId, "unit metric", ewrsUnitsMenuRoot, EWR.setUnitPreference, playerGroupId, EWR.unitInternationalSystem)

    local ewrsFrecuencyMenuRoot = missionCommands.addSubMenuForGroup(
            playerGroupId, "Frequency", ewrsMenuRoot)
    missionCommands.addCommandForGroup(
            playerGroupId, "high frequency reports", ewrsFrecuencyMenuRoot, EWR.setReportFrequencyPreference, playerGroupId, EWR.highReportFrequencyPreference)
    missionCommands.addCommandForGroup(
            playerGroupId, "default frequency reports", ewrsFrecuencyMenuRoot, EWR.setReportFrequencyPreference, playerGroupId, EWR.defaultReportFrequencyPreference)
    missionCommands.addCommandForGroup(
            playerGroupId, "low frequency reports", ewrsFrecuencyMenuRoot, EWR.setReportFrequencyPreference, playerGroupId, EWR.lowReportFrequencyPreference)

    EWR.radioMenusAdded[playerGroupId] = true
end

-- reschedule for users that change slot or coalition
setupClientScheduler = SCHEDULER:New(nil,
        function()
            EWR.setupClients()
        end, {}, 0, 30, 0)

-- cleanup schedulers of users that are dead or left the mission
cleanupScheduler = SCHEDULER:New(nil,
        function()
            for groupId, clientPreferences in pairs(EWR.playerEwrsPreferences) do
                local clientName = clientPreferences["clientName"]
                local client = CLIENT:FindByName(clientName, nil, false)
                if client == nil or not client:IsPlayer() then
                    if EWR.debug then
                        env.info(string.format("EWR client clean up for %s", groupId))
                    end
                    local scheduler = EWR.getReportSchedulerForPlayerGroup(groupId, clientPreferences)
                    scheduler:Stop()
                    env.info(string.format("EWR stopping scheduler for %s, %s", clientPreferences["groupId"], clientPreferences["clientName"]))
                    EWR.playerEwrsPreferences[groupId] = nil
                end
            end
        end, {}, 0, 30, 0)

env.info("Big Eye EWR " .. EWR.version .. " Running")
trigger.action.outText("Big Eye EWR " .. EWR.version .. " Running", 10)