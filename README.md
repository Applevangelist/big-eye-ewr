# Big Eye EWR

Big Eye is an EWR implementation for DCS.

## Requirements
requires MOOSE (at least 2.9.3) to run

download -> https://github.com/FlightControl-Master/MOOSE

## How to use
Use F10 menu to interact with Big Eye.
On the F10 root menu we can use `EWR Contact Report` to get an on-demand report

An additional submenu `EWR Preferences` allows personalization of different options.

## Mission Editor Requirements
Due to DCS limitations there should be one Client unit per DCS Group.

There is a limitation in DCS that makes it impossible to have text reports sent to client, instead they can only be sent to groups.

In order to clean schedulers for clients that leave the game or change unit, there is a cleanup system that removes scheduler.
Again, because of DCS limitations, these schedulers target DCS Groups, not DCS Units.

So, if one member of a group leaves the mission, then the other member(s) of the group will stop receiving reports.

### EWR, AWACS and SAM unit naming
Name your DCS groups and units accordingly to the prefixes:

```
EWR.blueCoalitionRadarPrefixes = { "BLUE AWACS", "BLUE CVN GROUP", "BLUE SAM", "BLUE EWR" }
EWR.redCoalitionRadarPrefixes = { "RED AWACS", "RED SAM", "RED EWR" }
```

These prefixes can be customized, but keep group names consistent.
For more details, review carefully the class `Core.Set` in Moose, specifically in the **Notes of FilterPrefixes**

For more information:
https://flightcontrol-master.github.io/MOOSE_DOCS/Documentation/Core.Set.html

## Feature Set
* uses in-game units to detect enemy airborne units
* players can configure report reference (self, bullseye), report frequency, report format details and opt-in/out of automated reports
* detected units are sorted by threat level
* bogey dope
* friendly picture

## Planned
* merge detector
* switch to 12-hour format for reporting when units closer than 5 miles
* focus closest unit after merge and report will focus on closest unit
* GCI mode

### Credits
* new implementation by quelcertoleo
* original EWRS script updated by Apple 30/11/2023
* original EWRS concept by Steggles - https://github.com/Bob7heBuilder/EWRS